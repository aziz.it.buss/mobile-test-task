import 'package:flutter_secure_storage/flutter_secure_storage.dart';
import 'dart:developer' as developer;

class MyStorage {
  final _storage = const FlutterSecureStorage();

  static const _accessToken = 'AccessTOKEN';
  static const _id = 'ID';
  static const _email = 'Email';
  static const _name = 'Name';
  static const _mobileNumber = 'MobileNumber';

  ///////////////////////////////////////
  Future<void> saveAccessToken(String accessToken) async {
    await _storage.write(key: _accessToken, value: accessToken);
    developer.log('accessToken: $accessToken saved now in secure_storage');
  }

  Future<bool> hasAccessToken() async {
    var value = await _storage.read(key: _accessToken);
    return value != null;
  }

  Future<String?> getAccessToken() async => _storage.read(key: _accessToken);

  Future<void> saveCustomerID(String customerID) async {
    await _storage.write(key: _id, value: customerID);
    developer.log('customerID: $customerID saved now in secure_storage');
  }

  Future<String?> getCustomerID() async => _storage.read(key: _id);

///////////////////////////////////////

  Future<void> saveEmail(String email) async {
    await _storage.write(key: _email, value: email);
    developer.log('email: $email saved now in secure_storage');
  }

  Future<String?> getEmail() async => _storage.read(key: _email);

///////////////////////////////////////

  Future<void> saveCustomerName(String customerName) async {
    await _storage.write(key: _name, value: customerName);
    developer.log('customerName: $customerName saved now in secure_storage');
  }

  Future<String?> getCustomerName() async => _storage.read(key: _name);

///////////////////////////////////////

  Future<void> saveMobileNumber(String mobileNumber) async {
    await _storage.write(key: _mobileNumber, value: mobileNumber);
    developer.log('mobileNumber: $mobileNumber saved now in secure_storage');
  }

  Future<String?> getMobileNumber() async => _storage.read(key: _mobileNumber);

///////////////////////////////////////

  Future<void> deleteAll() async {
    await _storage.deleteAll();
  }
}
