import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/storage/storage_controller.dart';

// import '../custom_errors.dart' as custom_errors;

import 'api.dart';
import 'interceptors/colorize_logger_interceptor.dart';
import 'interceptors/error_interceptors.dart';
import 'interceptors/response_interceptor.dart';

class ApiClient {
  final dio = createDio();
  ApiClient._internal();

  static final _singleton = ApiClient._internal();

  factory ApiClient() => _singleton;

  static Dio createDio() {
    var dio = Dio(
      BaseOptions(
        baseUrl: Api.baseUrl,
        receiveTimeout: 20000, // 15 seconds
        connectTimeout: 20000,
        sendTimeout: 20000,
      ),
    );
    dio.interceptors.addAll({
      ColorizeLoggerDioInterceptor(
        logRequestTimeout: false,
        logRequestHeaders: true,
        logResponseHeaders: false,
        // Optional, defaults to the 'log' function in the 'dart:developer' package.
      ),
      QueuedInterceptorsWrapper(onRequest: (options, handler) async {
        bool x = options.extra['token'] ?? true;
        // String? customToken = options.extra['customToken'];
        if (x) {
          MyStorage storage = Get.find();
          var token = await storage.getAccessToken();
          if (token != null) {
            options.headers['Authorization'] = 'Bearer $token';
          }
        }

        return handler.next(options);
      }),
      // RequestInterceptors(),
      ResponseInterceptors(dio),
      ErrorInterceptors(),
    });
    return dio;
  }
}
