import 'package:dio/dio.dart';
import 'package:mobile_test_task/Core/storage/storage_controller.dart';

import '../custom_errors.dart' as custom_errors;

class ResponseInterceptors extends Interceptor {
  Dio dio;

  MyStorage secureStorage = MyStorage();
  ResponseInterceptors(this.dio);
  @override
  void onResponse(Response response, ResponseInterceptorHandler handler) async {
    // print(response.statusMessage);
    // print(response.data);
    if (response.data['success'] != true) {
      handler.reject(
        custom_errors.CustomException(response.requestOptions, response),
        true,
      );
    } else {
      return handler.next(response);
    }
  }
}
