import 'package:dio/dio.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/storage/storage_controller.dart';

import '../custom_errors.dart' as custom_errors;

class ErrorInterceptors extends Interceptor {
  MyStorage storage = Get.find();

  ErrorInterceptors();
  static const String unhandledException =
      'Something went wrong, please try again';

  @override
  void onError(DioError err, ErrorInterceptorHandler handler) {
    switch (err.type) {
      case DioErrorType.connectTimeout:
      case DioErrorType.sendTimeout:
      case DioErrorType.receiveTimeout:
        throw custom_errors.DeadlineExceededException(err.requestOptions);
      case DioErrorType.response:
        switch (err.response?.statusCode) {
          case 400:
            if (err.response != null) {
              switch (err.response!.data['response']) {
                default:
                  throw custom_errors.BadRequestException(err.requestOptions);
              }
            } else {
              throw custom_errors.BadRequestException(err.requestOptions);
            }

          case 401:
            throw custom_errors.UnauthorizedException(
                err.requestOptions, err.response);
          // if (err.response != null &&
          //     err.response!.data != "" &&
          //     err.response!.data != null) {
          //   switch (err.response!.data['response']) {
          //     case 13:
          //       // BlocProvider.of<AuthenticationCubit>(
          //       //         MyApp.navigatorKey.currentState!.context)
          //       //     .clear();
          //       // Navigator.of(MyApp.navigatorKey.currentState!.context)
          //       //     .pushNamedAndRemoveUntil(IntroScreen.routeName,
          //       //         (Route<dynamic> route) => false);
          //       throw custom_errors.UnauthorizedException(
          //           err.requestOptions, err.response);
          //     case 14:
          //       throw InvalidLogin(err.requestOptions, err.response);
          //     default:
          //       throw custom_errors.UnauthorizedException(
          //           err.requestOptions, err.response);
          //   }
          // } else {
          //   return;
          //   //  _handle401(err, handler);
          // }
          case 403:
            throw custom_errors.UnauthorizedException(
                err.requestOptions, err.response);
          case 404:
            throw custom_errors.NotFoundException(err.requestOptions);
          case 409:
            throw custom_errors.ConflictException(err.requestOptions);
          case 500:
            throw custom_errors.InternalServerErrorException(
                err.requestOptions, err.response);
          default:
            throw custom_errors.InternalServerErrorException(
                err.requestOptions, err.response);
        }
      // break;
      case DioErrorType.cancel:
        break;
      case DioErrorType.other:
        throw custom_errors.NoInternetConnectionException(err.requestOptions);
    }
    return handler.next(err);
  }
}
