import 'package:dio/dio.dart';
import 'package:mobile_test_task/Core/apis/api.dart';
import 'package:mobile_test_task/Core/apis/api_client.dart';
import 'package:mobile_test_task/Core/models/register_model.dart';

class AuthRepo {
  Future<RegisterModel?> register({
    required String fullName,
    required String mobile,
    required String email,
    required String password,
  }) async {
    Map<String, dynamic> data = {
      'name': fullName,
      'email': email,
      'phone': mobile,
      'password': password,
      'country_code': '+971',
      'password_confirm': password,
    };
    FormData formData = FormData.fromMap(data);
    final response = await ApiClient().dio.post(Api.register, data: formData);
    return RegisterModel.fromJson(response.data);
  }

  Future<RegisterModel?> login({
    required String email,
    required String password,
  }) async {
    Map<String, dynamic> data = {
      'email': email,
      'password': password,
    };
    FormData formData = FormData.fromMap(data);
    final response = await ApiClient().dio.post(Api.login, data: formData);
    return RegisterModel.fromJson(response.data);
  }
}
