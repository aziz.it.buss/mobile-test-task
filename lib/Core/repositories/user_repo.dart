import 'package:dio/dio.dart';

import '../Apis/api.dart';
import '../apis/api_client.dart';

class UserRepo {
  Future<int?> update({
    required String fullName,
    required String mobile,
    required String email,
  }) async {
    Map<String, dynamic> data = {
      'name': fullName,
      'email': email,
      'phone': mobile,
      'country_code': '+971',
    };
    FormData formData = FormData.fromMap(data);
    final response = await ApiClient().dio.post(Api.updateInfo, data: formData);
    return response.statusCode;
  }
}
