import 'dart:developer';

import 'package:get/get.dart';
import 'package:mobile_test_task/Core/storage/storage_controller.dart';
import 'package:mobile_test_task/UI/views/welcome_screen/welcome_screen.dart';

enum AuthState { inital, loading, unAuth, auth, failed }

class AuthController extends GetxController {
  Rx<AuthState> state = AuthState.inital.obs;
  RxString error = ''.obs;

  Future<void> checkForLogin() async {
    state.value = AuthState.loading;
    log('Auth Loading');
    MyStorage storage = Get.find();
    bool hasToken = await storage.hasAccessToken();
    await Future.delayed(const Duration(seconds: 1));
    if (hasToken) {
      state.value = AuthState.auth;
      log('Authinaticated');
    } else {
      state.value = AuthState.unAuth;
      log('Unauthinaticated');
    }
  }

  void logout() {
    MyStorage storage = Get.find();
    storage.deleteAll();
    Get.offAll(const WelcomeScreen());
  }
}
