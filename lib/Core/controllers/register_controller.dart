import 'dart:developer';

import 'package:dio/dio.dart';

import 'package:get/get.dart';
import 'package:mobile_test_task/Core/repositories/auth_repo.dart';
import 'package:mobile_test_task/Core/storage/storage_controller.dart';
import 'package:mobile_test_task/UI/helpers/easy_loading_config.dart';
import 'package:mobile_test_task/UI/views/home_screen.dart/home_screen.dart';

class RegisterController extends GetxController {
  Future<void> register({
    required String fullName,
    required String mobile,
    required String email,
    required String password,
  }) async {
    Loader.show(status: EasyLoadingConfig.loadingText);
    try {
      AuthRepo repo = Get.find();
      final response = await repo.register(
          fullName: fullName, mobile: mobile, email: email, password: password);
      Loader.dismiss();
      MyStorage storage = Get.find();
      storage.saveAccessToken(response!.data.token);
      storage.saveCustomerName(response.data.name);
      storage.saveMobileNumber(response.data.phone);
      storage.saveEmail(response.data.email);
      Get.offAllNamed(HomeScreen.routeName);
    } on DioError catch (e) {
      Loader.dismiss();
      Get.defaultDialog(
          title: 'Error !', middleText: e.toString(), textCancel: 'Ok');
    } catch (e) {
      Loader.dismiss();
      log(e.toString());
      Get.defaultDialog(
          title: 'Error !', middleText: e.toString(), textCancel: 'Ok');
    }
  }
}
