import 'dart:developer';

import 'package:dio/dio.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/controllers/home_controller.dart';
import 'package:mobile_test_task/Core/repositories/user_repo.dart';
import 'package:mobile_test_task/UI/helpers/easy_loading_config.dart';

import '../storage/storage_controller.dart';

class UpdateInfoController extends GetxController {
  Future<void> updateInfo({
    required String email,
    required String name,
    required String mobile,
  }) async {
    Loader.show(status: EasyLoadingConfig.loadingText);
    try {
      UserRepo repo = Get.find();

      await repo.update(email: email, fullName: name, mobile: mobile);
      Loader.dismiss();
      MyStorage storage = Get.find();

      storage.saveCustomerName(name);
      storage.saveMobileNumber(mobile);
      storage.saveEmail(email);
      Get.find<HomeContoller>()
          .updateInfo(email: email, name: name, mobile: mobile);
      Get.back();
      Get.showSnackbar(const GetSnackBar(
        title: 'Success',
        message: 'Your inforamtion is updated successfully',
        backgroundColor: Colors.green,
        duration: Duration(seconds: 2),
        margin: EdgeInsets.only(bottom: 10, left: 5, right: 5),
        borderRadius: 15,
      ));
    } on DioError catch (e) {
      Loader.dismiss();
      Get.defaultDialog(
          title: 'Error !', middleText: e.toString(), textCancel: 'Ok');
    } catch (e) {
      Loader.dismiss();
      log(e.toString());
      Get.defaultDialog(
          title: 'Error !', middleText: e.toString(), textCancel: 'Ok');
    }
  }
}
