import 'package:get/get.dart';
import 'package:mobile_test_task/Core/storage/storage_controller.dart';

class HomeContoller extends GetxController {
  RxString name = ''.obs;
  RxString mobile = ''.obs;
  RxString email = ''.obs;

  void init() async {
    MyStorage storage = Get.find();
    name.value = (await storage.getCustomerName())!;
    mobile.value = (await storage.getMobileNumber())!;
    email.value = (await storage.getEmail())!;
  }

  void updateInfo({
    required String email,
    required String name,
    required String mobile,
  }) {
    this.name.value = name;
    this.mobile.value = mobile;
    this.email.value = email;
  }
}
