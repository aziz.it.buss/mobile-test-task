import 'package:flutter/material.dart';
import 'package:flutter_easyloading/flutter_easyloading.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';
import 'package:mobile_test_task/UI/helpers/easy_loading_config.dart';
import 'package:mobile_test_task/UI/helpers/my_bindings.dart';
import 'package:mobile_test_task/UI/views/home_screen.dart/home_screen.dart';
import 'package:mobile_test_task/UI/views/login_screen/login_screen.dart';
import 'package:mobile_test_task/UI/views/register_screen/register_screen.dart';
import 'package:mobile_test_task/UI/views/update_info_screen/update_info_screen.dart';

import 'package:mobile_test_task/UI/views/welcome_screen/welcome_screen.dart';
import 'package:mobile_test_task/app_wrapper.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

void main() {
  runApp(const MyApp());
  EasyLoadingConfig.config();
}

class MyApp extends StatelessWidget {
  const MyApp({super.key});

  @override
  Widget build(BuildContext context) {
    return ResponsiveSizer(
      builder: (context, orientation, deviceType) => GetMaterialApp(
        title: 'Mobile Test Task',
        theme: appTheme,
        debugShowCheckedModeBanner: false,
        home: const AppWrapper(),
        builder: EasyLoading.init(),
        initialBinding: MyBindings(),
        getPages: [
          GetPage(name: '/', page: () => const WelcomeScreen()),
          GetPage(
              name: RegisterScreen.routeName,
              page: () => const RegisterScreen()),
          GetPage(name: LoginScreen.routeName, page: () => const LoginScreen()),
          GetPage(name: HomeScreen.routeName, page: () => const HomeScreen()),
          GetPage(
              name: UpdateInfoScreen.routeName,
              page: () => const UpdateInfoScreen()),
        ],
      ),
    );
  }
}
