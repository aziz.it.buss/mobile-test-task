import 'package:flutter/material.dart';

import 'package:mobile_test_task/UI/views/home_screen.dart/localWidgets/buttons_section.dart';
import 'package:mobile_test_task/UI/views/home_screen.dart/localWidgets/info_section.dart';
import 'package:mobile_test_task/UI/widgets/my_app_bar.dart';

class HomeScreen extends StatelessWidget {
  static const routeName = '/HomeScreen';
  const HomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: const MyAppBar(title: 'Home Page'),
      body: Column(
        children: [InfoSection(), ButtonsSection()],
      ),
    );
  }
}
