import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/controllers/auth_controller.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';
import 'package:mobile_test_task/UI/views/update_info_screen/update_info_screen.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

// ignore: must_be_immutable
class ButtonsSection extends StatelessWidget {
  ButtonsSection({super.key});
  AuthController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Column(
      children: [
        buildButtonTile('Update Information',
            () => {Get.toNamed(UpdateInfoScreen.routeName)}),
        buildButtonTile('Change Password', () => {}),
        buildButtonTile('Delete Account', () => {}),
        buildButtonTile('Logout', () => {controller.logout()}),
      ],
    );
  }

  Widget buildButtonTile(String label, Function ontap) {
    return Card(
      elevation: 2,
      shape: RoundedRectangleBorder(borderRadius: BorderRadius.circular(10.Q)),
      child: ListTile(
        onTap: () => {ontap()},
        title: Text(
          label,
          style: TextStyle(
              fontWeight: FontWeight.bold, fontSize: 18.sp, color: mainColor),
        ),
        trailing: const Icon(
          Icons.arrow_forward_ios_rounded,
          color: mainColor,
        ),
      ),
    );
  }
}
