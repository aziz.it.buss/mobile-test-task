import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/controllers/home_controller.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class InfoSection extends StatefulWidget {
  const InfoSection({super.key});

  @override
  State<InfoSection> createState() => _InfoSectionState();
}

class _InfoSectionState extends State<InfoSection> {
  HomeContoller contoller = Get.find();
  @override
  void initState() {
    contoller.init();
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return GetX<HomeContoller>(
      builder: (controller) => Column(
        children: [
          buildInfoTile(
              icon: Icons.person_outline, infoText: controller.name.value),
          buildInfoTile(
              icon: Icons.phone_android_outlined,
              infoText: controller.mobile.value),
          buildInfoTile(
              icon: Icons.email_outlined, infoText: controller.email.value)
        ],
      ),
    );
  }

  Widget buildInfoTile({required IconData icon, required String infoText}) {
    return ListTile(
      leading: Icon(
        icon,
        color: mainColor,
        size: 30.Q,
      ),
      title: Text(
        infoText,
        style: TextStyle(
            fontSize: 16.sp, fontWeight: FontWeight.bold, color: Colors.grey),
      ),
    );
  }
}
