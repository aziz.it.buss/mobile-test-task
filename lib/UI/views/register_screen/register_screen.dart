import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/controllers/register_controller.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';
import 'package:mobile_test_task/UI/views/login_screen/login_screen.dart';
import 'package:mobile_test_task/UI/widgets/button.dart';
import 'package:mobile_test_task/UI/widgets/input_field.dart';
import 'package:mobile_test_task/UI/widgets/our_logo.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class RegisterScreen extends StatefulWidget {
  static const routeName = '/RegisterScreen';

  const RegisterScreen({super.key});

  @override
  State<RegisterScreen> createState() => _RegisterScreenState();
}

class _RegisterScreenState extends State<RegisterScreen> {
  final emailController = TextEditingController();

  final nameController = TextEditingController();

  final mobileController = TextEditingController();

  final passwordController = TextEditingController();
  final confirmPasswordController = TextEditingController();
  final formKey = GlobalKey<FormState>();

  RegisterController controller = Get.find();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.only(top: 12.h, bottom: 1.h),
      child: SingleChildScrollView(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const OurLogo(labelText: 'Register'),
              SizedBox(
                height: 8.h,
              ),
              formSection(),
              SizedBox(
                height: 3.h,
              ),
              Button(
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      controller.register(
                          fullName: nameController.text,
                          mobile: mobileController.text,
                          email: emailController.text,
                          password: passwordController.text);
                    }
                  },
                  text: 'Register'),
              SizedBox(
                height: 6.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text('Already have an account? ',
                      style: Theme.of(context).textTheme.bodyMedium),
                  GestureDetector(
                    onTap: () {
                      // Navigator.of(context)
                      //     .pushReplacementNamed(LoginScreen.routeName);
                      Get.offNamed(LoginScreen.routeName);
                    },
                    child: Text(
                      'Login',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontWeight: FontWeight.w900, color: mainColor),
                    ),
                  ),
                ],
              )
            ]),
      ),
    ));
  }

  Widget formSection() {
    return Form(
        key: formKey,
        child: Column(
          children: [
            InputField(
              controller: nameController,
              hint: 'Full Name',
              formContext: context,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter your full name';
                }
                return null;
              },
            ),
            SizedBox(
              height: 1.5.h,
            ),
            InputField(
              controller: mobileController,
              hint: '55994435',
              showFlag: true,
              formContext: context,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter your mobile number';
                }
                if (value.length != 10) {
                  return 'Must consist of 10 numbers';
                }
                return null;
              },
            ),
            SizedBox(
              height: 1.5.h,
            ),
            InputField(
              controller: emailController,
              hint: 'Email Address',
              formContext: context,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter your email';
                }
                return null;
              },
            ),
            SizedBox(
              height: 1.5.h,
            ),
            InputField(
              controller: passwordController,
              hint: 'Password',
              formContext: context,
              showEye: true,
              obsecure: true,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter your password';
                }
                return null;
              },
            ),
            SizedBox(
              height: 1.5.h,
            ),
            InputField(
              controller: confirmPasswordController,
              hint: 'Confirm Password',
              formContext: context,
              showEye: true,
              obsecure: true,
              validator: (value) {
                if (value!.isEmpty) {
                  return 'Please enter your password again';
                }
                if (value != passwordController.text) {
                  return 'Please enter the same password';
                }
                return null;
              },
            )
          ],
        ));
  }
}
