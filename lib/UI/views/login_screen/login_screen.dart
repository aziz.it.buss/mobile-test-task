import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/controllers/login_controller.dart';

import 'package:mobile_test_task/UI/views/register_screen/register_screen.dart';
import 'package:mobile_test_task/UI/widgets/input_field.dart';
import 'package:mobile_test_task/UI/widgets/our_logo.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

import '../../helpers/appStyle.dart';
import '../../widgets/button.dart';

class LoginScreen extends StatefulWidget {
  static const routeName = '/LoginScreen';
  const LoginScreen({super.key});

  @override
  State<LoginScreen> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  final emailController = TextEditingController();

  final passwordController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  LoginController controller = Get.find();

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.only(top: 12.h, bottom: 1.h),
      child: SingleChildScrollView(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: [
              const OurLogo(labelText: 'Login'),
              SizedBox(
                height: 18.h,
              ),
              Form(
                  key: formKey,
                  child: Column(
                    children: [
                      InputField(
                        controller: emailController,
                        hint: 'Email Address',
                        formContext: context,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter your email';
                          }
                          return null;
                        },
                      ),
                      SizedBox(
                        height: 1.5.h,
                      ),
                      InputField(
                        controller: passwordController,
                        hint: 'Password',
                        formContext: context,
                        showEye: true,
                        obsecure: true,
                        validator: (value) {
                          if (value!.isEmpty) {
                            return 'Please enter your password';
                          }
                          return null;
                        },
                      ),
                    ],
                  )),
              SizedBox(
                height: 5.h,
              ),
              Button(
                  onPressed: () {
                    if (formKey.currentState!.validate()) {
                      controller.login(
                          email: emailController.text,
                          password: passwordController.text);
                    }
                  },
                  text: 'Login'),
              SizedBox(
                height: 18.h,
              ),
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Text("Don't have an account? ",
                      style: Theme.of(context).textTheme.bodyMedium),
                  GestureDetector(
                    onTap: () {
                      // Navigator.of(context)
                      //     .pushReplacementNamed(RegisterScreen.routeName);
                      Get.offNamed(RegisterScreen.routeName);
                    },
                    child: Text(
                      'Register',
                      style: Theme.of(context).textTheme.bodyMedium!.copyWith(
                          fontWeight: FontWeight.w900, color: mainColor),
                    ),
                  ),
                ],
              )
            ]),
      ),
    ));
  }
}
