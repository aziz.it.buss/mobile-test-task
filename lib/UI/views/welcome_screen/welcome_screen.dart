import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';
import 'package:mobile_test_task/UI/views/login_screen/login_screen.dart';
import 'package:mobile_test_task/UI/views/register_screen/register_screen.dart';
import 'package:mobile_test_task/UI/widgets/button.dart';
import 'package:mobile_test_task/UI/widgets/our_logo.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class WelcomeScreen extends StatelessWidget {
  const WelcomeScreen({super.key});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: Padding(
      padding: EdgeInsets.only(top: 12.h, bottom: 1.h),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          const OurLogo(labelText: 'Welcome to the app'),
          SizedBox(
            height: 23.h,
          ),
          Button(
            onPressed: () {
              // Navigator.of(context).pushNamed(LoginScreen.routeName);
              Get.toNamed(LoginScreen.routeName);
            },
            text: 'Login',
          ),
          SizedBox(
            height: 1.h,
          ),
          Button(
            onPressed: () {
              // Navigator.of(context).pushNamed(RegisterScreen.routeName);
              Get.toNamed(RegisterScreen.routeName);
            },
            text: 'Register',
            buttonColor: Colors.white,
            textColor: mainColor,
            borderColor: mainColor,
          ),
          const Spacer(),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Text(
                'Designed & Developed by ',
                style: Theme.of(context).textTheme.bodyMedium,
              ),
              Text(
                'Ali Fouad',
                style: TextStyle(
                    fontSize: 16.sp,
                    fontWeight: FontWeight.bold,
                    color: mainColor,
                    decoration: TextDecoration.underline),
              ),
            ],
          )
        ],
      ),
    ));
  }
}
