import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/controllers/home_controller.dart';
import 'package:mobile_test_task/Core/controllers/update_info_controller.dart';
import 'package:mobile_test_task/UI/widgets/button.dart';
import 'package:mobile_test_task/UI/widgets/input_field.dart';
import 'package:mobile_test_task/UI/widgets/my_app_bar.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class UpdateInfoScreen extends StatefulWidget {
  static const routeName = '/UpdateInfo';
  const UpdateInfoScreen({super.key});

  @override
  State<UpdateInfoScreen> createState() => _UpdateInfoScreenState();
}

class _UpdateInfoScreenState extends State<UpdateInfoScreen> {
  final emailController = TextEditingController();

  final nameController = TextEditingController();

  final mobileController = TextEditingController();

  final formKey = GlobalKey<FormState>();

  HomeContoller controller = Get.find();
  UpdateInfoController updateController = Get.find();

  @override
  void initState() {
    emailController.text = controller.email.value;
    nameController.text = controller.name.value;
    mobileController.text = controller.mobile.value;
    super.initState();
  }

  void unfocusKeyboard(BuildContext context) {
    FocusScope.of(context).unfocus();
  }

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
        onTap: () {
          // Call the function to unfocus the keyboard when a tap occurs outside the input elements.
          unfocusKeyboard(context);
        },
        child: Scaffold(
          appBar: const MyAppBar(
            title: 'Update Information',
            isHome: false,
          ),
          body: SingleChildScrollView(
            child: Center(
              child: Padding(
                padding: EdgeInsets.only(top: 2.h),
                child: Column(
                    crossAxisAlignment: CrossAxisAlignment.center,
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Form(
                          key: formKey,
                          child: Column(children: [
                            InputField(
                              controller: nameController,
                              hint: 'Full Name',
                              formContext: context,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Please enter your full name';
                                }
                                return null;
                              },
                            ),
                            SizedBox(
                              height: 1.5.h,
                            ),
                            InputField(
                              controller: mobileController,
                              hint: '55994435',
                              showFlag: true,
                              formContext: context,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Please enter your mobile number';
                                }
                                if (value.length != 10) {
                                  return 'Must consist of 10 numbers';
                                }
                                return null;
                              },
                            ),
                            SizedBox(
                              height: 1.5.h,
                            ),
                            InputField(
                              controller: emailController,
                              hint: 'Email Address',
                              formContext: context,
                              validator: (value) {
                                if (value!.isEmpty) {
                                  return 'Please enter your email';
                                }
                                return null;
                              },
                            ),
                          ])),
                      SizedBox(
                        height: 3.h,
                      ),
                      Button(
                          onPressed: () {
                            if (formKey.currentState!.validate()) {
                              updateController.updateInfo(
                                  email: emailController.text,
                                  name: nameController.text,
                                  mobile: mobileController.text);
                            }
                          },
                          text: 'Save'),
                    ]),
              ),
            ),
          ),
        ));
  }
}
