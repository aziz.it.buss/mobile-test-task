import 'package:flutter/material.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

//I have created InputField widget to make the code more cleaner and so it can be reusable too!
//I use this widget in every project that I develop :)

// ignore: must_be_immutable
class InputField extends StatefulWidget {
  final String? label;
  final String? hint;
  final TextEditingController controller;
  final BuildContext? formContext;
  bool obsecure;
  final bool showEye;
  final IconData? icon;
  final bool withBorder;
  final FormFieldValidator<String>? validator;
  final bool multiLine;
  final TextInputType keybordType;
  final String? valMessage;
  final bool showFlag;
  InputField({
    Key? key,
    this.label,
    required this.controller,
    this.obsecure = false,
    this.formContext,
    this.valMessage,
    this.withBorder = false,
    this.validator,
    this.showEye = false,
    this.icon,
    this.hint,
    this.multiLine = false,
    this.keybordType = TextInputType.text,
    this.showFlag = false,
  }) : super(key: key);

  @override
  State<InputField> createState() => _InputFieldState();
}

class _InputFieldState extends State<InputField> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: 70.w,
      child: TextFormField(
        onEditingComplete: widget.formContext == null
            ? null
            : () {
                FocusScope.of(widget.formContext!).nextFocus();
              },
        controller: widget.controller,
        obscureText: widget.obsecure,
        textAlignVertical: TextAlignVertical.center,
        textAlign: TextAlign.center,
        maxLines: widget.multiLine ? 3 : 1,
        keyboardType: widget.keybordType,
        style: TextStyle(fontSize: 16.sp, fontWeight: FontWeight.bold),
        cursorColor: mainColor,
        decoration: InputDecoration(
            contentPadding: widget.showEye
                ? EdgeInsets.only(left: 10.w)
                : widget.showFlag
                    ? EdgeInsets.only(right: 12.w)
                    : null,
            suffixIconColor: mainColor,
            prefixIcon: !widget.showFlag
                ? null
                : Padding(
                    padding: const EdgeInsets.only(left: 10.0),
                    child: SizedBox(
                      height: 0.5.h,
                      width: 1.w,
                      child: Image.asset('assets/images/flag.png'),
                    ),
                  ),
            labelText: widget.label,
            hintStyle: TextStyle(
                fontSize: 16.sp,
                fontWeight: FontWeight.w600,
                color: const Color.fromARGB(255, 132, 132, 132)),
            hintText: widget.hint,
            labelStyle: Theme.of(context).textTheme.labelSmall,
            suffixIcon: widget.showEye
                ? IconButton(
                    padding: const EdgeInsets.all(0),
                    onPressed: () {
                      setState(() {
                        widget.obsecure = !widget.obsecure;
                      });
                    },
                    splashRadius: 12,
                    icon: Icon(widget.obsecure
                        ? Icons.visibility
                        : Icons.visibility_off))
                : null,
            filled: true,
            fillColor: Colors.white,
            isDense: true,
            disabledBorder:
                OutlineInputBorder(borderRadius: BorderRadius.circular(20)),
            errorStyle: const TextStyle(
                fontSize: 10,
                height: 1,
                color: Colors.redAccent,
                fontWeight: FontWeight.bold),
            focusedErrorBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: Colors.red, width: 2),
                borderRadius: BorderRadius.circular(15.Q)),
            enabledBorder: OutlineInputBorder(
                borderSide: widget.withBorder
                    ? const BorderSide(color: Colors.black12)
                    : const BorderSide(color: Colors.grey, width: 2),
                borderRadius: BorderRadius.circular(15.Q)),
            errorBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: mainColor, width: 2),
                borderRadius: BorderRadius.circular(15.Q)),
            focusedBorder: OutlineInputBorder(
                borderSide: const BorderSide(color: mainColor, width: 2),
                borderRadius: BorderRadius.circular(15.Q)),
            border:
                OutlineInputBorder(borderRadius: BorderRadius.circular(15.Q))),
        validator: widget.validator ??
            (value) {
              if (value!.isEmpty) {
                return widget.valMessage;
              } else {
                return null;
              }
            },
      ),
    );
  }
}
