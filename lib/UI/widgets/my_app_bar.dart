import 'package:flutter/material.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class MyAppBar extends StatelessWidget implements PreferredSizeWidget {
  final String title;
  final bool isHome;
  const MyAppBar({super.key, required this.title, this.isHome = true});

  @override
  Widget build(BuildContext context) {
    return AppBar(
      backgroundColor: mainColor,
      title: Text(
        title,
        style: const TextStyle(fontWeight: FontWeight.bold),
      ),
      centerTitle: true,
      foregroundColor: Colors.white,
      leading: !isHome
          ? IconButton(
              onPressed: () {
                FocusScope.of(context).unfocus();
                Get.back();
              },
              icon: const Icon(Icons.arrow_back_ios_new_rounded))
          : null,
    );
  }

  @override
  Size get preferredSize => Size(100.w, 7.h);
}
