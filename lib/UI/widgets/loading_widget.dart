import 'package:flutter/material.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';

class LoadingWidget extends StatelessWidget {
  final double? size;
  final double? padding;
  const LoadingWidget({
    Key? key,
    this.size,
    this.padding,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.only(bottom: 10.0, top: padding ?? 0.0),
        child: const CircularProgressIndicator(
          color: mainColor,
        ));
  }
}
