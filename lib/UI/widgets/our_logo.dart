import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class OurLogo extends StatelessWidget {
  final String labelText;
  const OurLogo({super.key, required this.labelText});

  @override
  Widget build(BuildContext context) {
    return Center(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        children: [
          SizedBox(
            height: 16.h,
            child: Image.asset('assets/images/logo.png'),
          ),
          SizedBox(
            height: 1.5.h,
          ),
          Text(
            labelText,
            style: Theme.of(context).textTheme.labelLarge,
          )
        ],
      ),
    );
  }
}
