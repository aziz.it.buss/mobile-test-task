import 'package:flutter/material.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';

import 'package:responsive_sizer/responsive_sizer.dart';

//I have created button widget to make the code more cleaner and so it can be reusable too!
//I use this widget in every project that I develop :)
class Button extends StatelessWidget {
  final String? text;
  final Function()? onPressed;
  final double? buttonWidth;
  final double? buttonHight;
  final Color buttonColor;
  final Color textColor;
  final double? elevation;
  final Color? borderColor;
  final IconData? icon;
  final bool enable;
  const Button(
      {Key? key,
      this.text,
      required this.onPressed,
      this.buttonWidth,
      this.buttonHight,
      this.buttonColor = mainColor,
      this.textColor = Colors.white,
      this.elevation,
      this.borderColor,
      this.icon,
      this.enable = true})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SizedBox(
        width: buttonWidth ?? 70.w,
        height: 6.5.h,
        child: icon == null || (icon != null && text == null)
            ? ElevatedButton(
                style: TextButton.styleFrom(
                    elevation: elevation,
                    backgroundColor: buttonColor,
                    shape: RoundedRectangleBorder(
                        side: borderColor == null
                            ? BorderSide.none
                            : BorderSide(color: borderColor!, width: 2),
                        borderRadius: BorderRadius.circular(10.Q))),
                onPressed: enable ? onPressed : null,
                child: text == null
                    ? Icon(
                        icon,
                        color: textColor,
                        size: 28.Q,
                      )
                    : Text(
                        text!,
                        style: Theme.of(context)
                            .textTheme
                            .labelMedium!
                            .copyWith(color: textColor),
                      ))
            : ElevatedButton.icon(
                style: TextButton.styleFrom(
                  elevation: elevation,
                  backgroundColor: buttonColor,
                  shape: RoundedRectangleBorder(
                      side: borderColor == null
                          ? BorderSide.none
                          : BorderSide(
                              color: borderColor!,
                            ),
                      borderRadius: BorderRadius.circular(10.Q)),
                ),
                onPressed: enable ? onPressed : null,
                icon: Icon(
                  icon,
                  color: textColor,
                ),
                label: Text(
                  text!,
                )));
  }
}
