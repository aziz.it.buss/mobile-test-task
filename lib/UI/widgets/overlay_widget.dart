import 'package:flutter/material.dart';
import 'package:mobile_test_task/UI/helpers/appStyle.dart';
import 'package:mobile_test_task/UI/widgets/loading_widget.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

class CustomOverlayWidget extends StatelessWidget {
  const CustomOverlayWidget({super.key});

  @override
  Widget build(BuildContext context) {
    return Container(
      width: 200,
      height: 100,
      color: Colors.white,
      child: Center(
          child: Column(
        children: [
          const LoadingWidget(),
          SizedBox(
            height: 2.h,
          ),
          Text(
            'Loading..',
            style: TextStyle(
                fontWeight: FontWeight.bold, fontSize: 17.sp, color: mainColor),
          )
        ],
      )),
    );
  }
}
