import 'package:flutter/material.dart';
import 'package:responsive_sizer/responsive_sizer.dart';

const mainColor = Color(0xff3D0548);

ThemeData appTheme = ThemeData(
    primaryColor: mainColor,
    canvasColor: Colors.white,
    fontFamily: "AlexandriaFLF",
    textTheme: TextTheme(
        labelMedium: TextStyle(
            fontSize: 18.sp, fontWeight: FontWeight.bold, color: Colors.white),
        bodyMedium: TextStyle(
            fontSize: 16.sp,
            fontWeight: FontWeight.w600,
            color: mainColor.withOpacity(0.5)),
        labelLarge: TextStyle(
            wordSpacing: 0.5.w,
            fontSize: 22.sp,
            fontWeight: FontWeight.bold,
            color: mainColor)));
