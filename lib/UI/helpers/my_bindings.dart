import 'package:get/get.dart';
import 'package:mobile_test_task/Core/controllers/auth_controller.dart';
import 'package:mobile_test_task/Core/controllers/home_controller.dart';
import 'package:mobile_test_task/Core/controllers/login_controller.dart';
import 'package:mobile_test_task/Core/controllers/register_controller.dart';
import 'package:mobile_test_task/Core/controllers/update_info_controller.dart';
import 'package:mobile_test_task/Core/repositories/auth_repo.dart';
import 'package:mobile_test_task/Core/repositories/user_repo.dart';
import 'package:mobile_test_task/Core/storage/storage_controller.dart';

class MyBindings implements Bindings {
  @override
  void dependencies() {
    Get.put(MyStorage());
    Get.put(AuthController());
    Get.put(AuthRepo());
    Get.put(LoginController());
    Get.put(RegisterController());
    Get.put(UserRepo());
    Get.put(HomeContoller());
    Get.put(UpdateInfoController());
  }
}
