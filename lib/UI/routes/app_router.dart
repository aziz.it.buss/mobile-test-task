import 'package:flutter/material.dart';
import 'package:mobile_test_task/UI/views/home_screen.dart/home_screen.dart';
import 'package:mobile_test_task/UI/views/login_screen/login_screen.dart';
import 'package:mobile_test_task/UI/views/register_screen/register_screen.dart';
import 'package:mobile_test_task/UI/views/update_info_screen/update_info_screen.dart';
import 'package:mobile_test_task/UI/views/welcome_screen/welcome_screen.dart';

class AppRouter {
  static Route<dynamic> generateRoute(RouteSettings? settings) {
    switch (settings!.name) {
      case RegisterScreen.routeName:
        return MaterialPageRoute(builder: (_) => const RegisterScreen());
      // return GetPage(name: RegisterScreen.routeName, page: RegisterScreen);
      case HomeScreen.routeName:
        return MaterialPageRoute(builder: (_) => const HomeScreen());
      case LoginScreen.routeName:
        return MaterialPageRoute(builder: (_) => const LoginScreen());
      case UpdateInfoScreen.routeName:
        return MaterialPageRoute(builder: (_) => const UpdateInfoScreen());
      default:
        return MaterialPageRoute(builder: (_) => const WelcomeScreen());
    }
  }
}
