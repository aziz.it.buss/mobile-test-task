import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:get/get.dart';
import 'package:mobile_test_task/Core/controllers/auth_controller.dart';
import 'package:mobile_test_task/UI/views/home_screen.dart/home_screen.dart';
import 'package:mobile_test_task/UI/views/welcome_screen/welcome_screen.dart';
import 'package:mobile_test_task/UI/widgets/loading_widget.dart';

class AppWrapper extends StatefulWidget {
  const AppWrapper({Key? key}) : super(key: key);

  @override
  AppWrapperState createState() => AppWrapperState();
}

class AppWrapperState extends State<AppWrapper> {
  AuthController auth = Get.find();

  @override
  Widget build(BuildContext context) {
    SystemChrome.setPreferredOrientations([
      DeviceOrientation.portraitUp,
    ]);
    return GetX<AuthController>(
      initState: (state) {
        auth.checkForLogin();
      },
      builder: (controller) {
        if (controller.state.value == AuthState.loading) {
          return const Scaffold(
              body: Center(
            child: LoadingWidget(),
          ));
        } else if (controller.state.value == AuthState.unAuth) {
          return const WelcomeScreen();
        } else if (controller.state.value == AuthState.auth) {
          return const HomeScreen();
        } else if (controller.state.value == AuthState.failed) {
          return const Scaffold(
              body: Center(child: Text('AuthenticationFailed')));
        }
        return const WelcomeScreen();
      },
    );
  }
}

  //   BlocBuilder<AuthenticationCubit, AuthenticationState>(
  //     builder: (context, state) {
  //       if (state is AuthenticationUninitialized) {
  //       } else if (state is AuthenticationLoading) {
  //         return const Scaffold(
  //             body: Center(
  //           child: LoadingWidget(),
  //         ));
  //       } else if (state is AuthenticationUnauthenticated) {
  //         return const LoginScreen();
  //       } else if (state is AuthenticationAuthenticated) {
  //         return const HomeScreen();
  //       } else if (state is AuthenticationFailed) {
  //         return const Scaffold(
  //             body: Center(child: Text('AuthenticationFailed')));
  //       }
  //       return const LoginScreen();
  //     },
  //   );
  // }

